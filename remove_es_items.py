#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#


import argparse
import elasticsearch as es
import elasticsearch_dsl as dsl
from elasticsearch_dsl import Q


def main():
    args = parse_args()
    host = args.host
    index = args.index
    field = args.field
    items = args.items
    
    delete_items(host, index, field, items)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("host",
                        help="ES host")
    parser.add_argument("index",
                        help="ES index name")
    parser.add_argument("field",
                        help="field of the ES index")
    parser.add_argument("items",
                        nargs="+",
                        help="list of values in the ES index field to remove")
    args = parser.parse_args()

    return args


def delete_items(host, index, field, items):
    client = es.Elasticsearch([host])

    s = dsl.Search(using=client, index=index)

    q = Q('terms')
    q.__setattr__(field, items)
    s = s.filter(q)

    response = s.delete()
    print(response)


if __name__ == '__main__':
    main()
