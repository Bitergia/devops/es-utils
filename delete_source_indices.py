#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import logging
import urllib3

import requests


DEFAULT_SLEEP_TIME = 1
MAX_RETRIES = 5

HEADER_KEY = "Content-Type"
HEADER_VALUE = "application/json"

# Logging formats
LOG_FORMAT = "[%(asctime)s - %(levelname)s] - %(message)s"


logger = logging.getLogger(__name__)


def main():
    args = parse_args()

    os_url = args.url
    insecure = args.insecure

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    indices_filename = args.input
    indices_list = read_json(indices_filename)

    for indices in indices_list:
        source_index = indices["source"]
        logger.info("Removing {}".format(source_index))
        try:
            delete_index(os_url, source_index, insecure)
            logger.info("Done")
        except requests.exceptions.HTTPError as e:
            logger.error(e)


def parse_args():
    description = """
    Reindex a list of source indices to destination indices. Data, mappings,
    and aliases (optional) are copied. All but the index settings.
    """
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=description)

    parser.add_argument("url",
                        help="OpenSearch URL")
    parser.add_argument("input",
                        help="Indices JSON file")
    parser.add_argument("--insecure",
                        action='store_true',
                        help="Disable SSL verification")
    parser.add_argument("-g", "--debug",
                        action='store_true',
                        help="Debug mode")
    args = parser.parse_args()
    return args


def read_json(file_name):
    with open(file_name, "r") as f:
        return json.loads(f.read())


def delete_index(os_url, index, insecure):
    headers = {HEADER_KEY: HEADER_VALUE}
    session = create_session(headers, insecure)
    url = os_url + "/" + index
    r = session.delete(url)
    r.raise_for_status()
    logger.debug(r.json())


def create_session(headers, insecure=False):
    """Create a Requests session"""

    session = requests.Session()
    session.headers.update(headers)

    retries = urllib3.util.Retry(total=MAX_RETRIES,
                                 connect=MAX_RETRIES,
                                 status=MAX_RETRIES,
                                 backoff_factor=DEFAULT_SLEEP_TIME,
                                 raise_on_status=True)

    session.mount('http://', requests.adapters.HTTPAdapter(max_retries=retries))
    session.mount('https://', requests.adapters.HTTPAdapter(max_retries=retries))

    if insecure:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session.verify = False

    return session


if __name__ == '__main__':
    main()
