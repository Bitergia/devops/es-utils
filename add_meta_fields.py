#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import logging


# Logging formats
LOG_FORMAT = "[%(asctime)s - %(levelname)s] - %(message)s"


logger = logging.getLogger(__name__)


def main():
    args = parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    logger.info("Add meta fields to projects.json")

    fields = args.fields
    meta_fields = create_meta_fields(fields)
    logger.info("meta fields to add: {}".format(meta_fields))

    projects_filename = args.projects
    projects = read_json(projects_filename)

    for project in projects:
        logger.info("project: {}".format(project))
        add_meta_field(projects[project], meta_fields)

    write_json(projects_filename, projects)
    logger.info("Done")


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("projects",
                        help="projects.json JSON file")
    parser.add_argument("fields",
                        nargs="+",
                        help="New fields ([KEY:VALUE KEY:VALUE ...])")
    parser.add_argument("-g", "--debug",
                        action='store_true',
                        help="Debug mode")
    args = parser.parse_args()
    return args


def read_json(file_name):
    with open(file_name, "r") as f:
        return json.loads(f.read())


def create_meta_fields(fields):
    meta_fields = {}
    for field in fields:
        key = field.split(":")[0]
        value = field.split(":")[1]
        meta_fields[key] = value

    return meta_fields


def add_meta_field(data, meta_fields):
    if "meta" not in data:
        data["meta"] = meta_fields
        logger.debug("Created meta: {}".format(meta_fields))
    else:
        data["meta"].update(meta_fields)
        logger.debug("Add new meta fields: {}".format(meta_fields))


def write_json(file_name, data):
    with open(file_name, "w+") as f:
        f.write(json.dumps(data, indent=4))


if __name__ == '__main__':
    main()
