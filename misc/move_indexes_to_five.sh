#!/bin/bash

# edit the following line at your needs
MAPPINGS_PATH="/root/gitlab_devops/GrimoireLab-mappings"

# don't edit below this line unless you know what you are doing

CREDENTIALS=$1
FROM=$2
TO=$3
RAW_TO=$4


RED='\033[0;31m'
NC='\033[0m' # No Color

raw_indexes=''
enr_indexes=''
data_sources=''

function display_usage() {
    echo "This script migrates data from ES2 to ES5 using index_warrior.py, elasticdump and a mappings directory."
    echo -e "\nUsage:\n$0 [credentials_file] [origin_es] [destination_enriched_indexes] [destination_raw_indexes]\n"
    echo "This is an example of credential file:"
    echo -e "USER=\"carmen\"\nPASS=\"password\""
}

function get_raw_index_names(){
    raw_indexes=`curl -s -XGET $ORIGIN_ES/_cat/aliases|awk '{print $2}'|uniq|grep -v enrich`
}

function get_enriched_index_names(){
    enr_indexes=`curl -s -XGET $ORIGIN_ES/_cat/aliases|awk '{print $2}'|uniq|grep enrich`
}

function get_ds_names(){
    data_sources=`curl -s -XGET $ORIGIN_ES/_cat/aliases|awk '{print $2}'|uniq|grep enrich|awk -F'_' '{print $1}'`
}

## main

if [  $# -le 0 ]
then
    display_usage
    exit 1
fi

if [[ ( $1 == "--help") ||  $1 == "-h" ]]
then
    display_usage
    exit 0
fi

if [ -f $CREDENTIALS ]
then
    source $CREDENTIALS
else
    echo "Credentials file '$1' not found"
    exit 1
fi

ORIGIN_ES="https://$USER:$PASS@$FROM/data"
DEST_ES="https://$USER:$PASS@$TO/data"
ALEXANDRIA_ES="https://$USER:$PASS@$RAW_TO/data"

get_raw_index_names
get_enriched_index_names
get_ds_names

echo "Uploading mapping for enriched indexes .."

missing_mappings=0
for d in $data_sources
do
    if [ ! -f $MAPPINGS_PATH/$d.json ]; then
        echo -e "  ${RED}missing${NC} mapping for $d"
        missing_mappings=1
    else
        for e in $enr_indexes
        do
            prefix=`echo $e|awk -F'_' '{print $1}'`
            if [ $prefix == $d ]
            then
                echo "  copying mapping $d to $DEST_ES/$e"
                echo "elasticdump --input=$MAPPINGS_PATH/$d.json --output=$DEST_ES --output-index=$e --type=mapping"|sh
            fi
        done
    fi
done

if [ $missing_mappings -eq 1 ]
then
    echo -e "\nPlease place the missing mappings at $MAPPINGS_PATH before continuing"
    exit 0
fi


echo "Copying enriched indexes .."

for e in $enr_indexes
do
  echo "  copying $e to $DEST_ES"
  echo "elasticdump --input=$ORIGIN_ES --input-index=$e --output=$DEST_ES --output-index=$e --limit=1000"|sh
done

echo "Copying raw indexes .."
for r in $raw_indexes
do
  echo "  copying $r to $ALEXANDRIA_ES"
  echo "elasticdump --input=$ORIGIN_ES --input-index=$r --output=$ALEXANDRIA_ES --output-index=$r --type=mapping"|sh
  echo "elasticdump --input=$ORIGIN_ES --input-index=$r --output=$ALEXANDRIA_ES --output-index=$r --limit=1000 --type=data"|sh
done
