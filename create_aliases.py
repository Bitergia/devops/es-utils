#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import logging
import urllib3

import requests


DEFAULT_SLEEP_TIME = 1
MAX_RETRIES = 5

HEADER_KEY = "Content-Type"
HEADER_VALUE = "application/json"

ALIAS_URL = "/_alias"
ALIASES_URL = "/_aliases"

# Logging formats
LOG_FORMAT = "[%(asctime)s - %(levelname)s] - %(message)s"


logger = logging.getLogger(__name__)


def main():
    args = parse_args()
    os_url = args.url
    tenant = args.tenant
    indices_filename = args.input
    insecure = args.insecure

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    indices_list = read_json(indices_filename)

    for indices in indices_list:
        source = indices["source"]
        dest = indices["dest"]
        logger.info("Create alias for {} tenant and {} index".format(tenant, dest))

        aliases = get_aliases(os_url, source, insecure)
        if "aliases" not in aliases:
            logger.error("There is no alias: {}".format(aliases))
            continue

        query = compose_query(aliases["aliases"], tenant, dest)
        logger.debug("Create alias query:\n{}".format(json.dumps(query, indent=4)))

        create_alias(os_url, query, insecure)
        logger.info("Done")


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("url",
                        help="OpenSearch URL")
    parser.add_argument("tenant",
                        help="Tenant name: The new alias will add this as a prefix")
    parser.add_argument("input",
                        help="Indices JSON file")
    parser.add_argument("--insecure",
                        action='store_true',
                        help="Disable SSL verification")
    parser.add_argument("-g", "--debug",
                        action='store_true',
                        help="Debug mode")
    args = parser.parse_args()
    return args


def read_json(file_name):
    with open(file_name, "r") as f:
        return json.loads(f.read())


def get_aliases(os_url, index, insecure):
    aliases = []

    headers = {HEADER_KEY: HEADER_VALUE}
    session = create_session(headers, insecure)

    url = os_url + ALIAS_URL
    r = session.get(url)
    data = r.json()
    if index in data and data[index]["aliases"]:
        aliases = data[index]

    return aliases


def compose_query(aliases, tenant, new_index):
    query = {
        "actions": []
    }
    for alias in aliases:
        new_alias = "{}_{}".format(tenant, alias)
        query_add = {
            "add": {
                "index": new_index,
                "alias": new_alias
            }
        }
        if "filter" in aliases[alias]:
            query_add["add"]["filter"] = aliases[alias]["filter"]

        query["actions"].append(query_add)

    return query


def create_alias(url, query, insecure):
    headers = {HEADER_KEY: HEADER_VALUE}
    session = create_session(headers, insecure)
    update_aliases_url = url + ALIASES_URL
    r = session.post(update_aliases_url, data=json.dumps(query))
    logger.debug(r.json())


def create_session(headers, insecure=False):

    session = requests.Session()
    session.headers.update(headers)

    retries = urllib3.util.Retry(total=MAX_RETRIES,
                                 connect=MAX_RETRIES,
                                 status=MAX_RETRIES,
                                 backoff_factor=DEFAULT_SLEEP_TIME,
                                 raise_on_status=True)

    session.mount('http://', requests.adapters.HTTPAdapter(max_retries=retries))
    session.mount('https://', requests.adapters.HTTPAdapter(max_retries=retries))

    if insecure:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session.verify = False

    return session


if __name__ == '__main__':
    main()
