#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import logging
import urllib3

import requests


DEFAULT_SLEEP_TIME = 1
MAX_RETRIES = 5

HEADER_KEY = "Content-Type"
HEADER_VALUE = "application/json"

REINDEX_URL = "{}/_reindex?wait_for_completion=false"

QUERY = """
{
    "source": {
        "index": "%s"
    },
    "dest": {
        "index": "%s"
    } %s
}
"""
SCRIPT = """
    ,"script": {
        "source": "%s"
    }
"""
SCRIPT_SOURCE = "ctx._source.%s = '%s'; "

# Logging formats
LOG_FORMAT = "[%(asctime)s - %(levelname)s] - %(message)s"


logger = logging.getLogger(__name__)


def main():
    args = parse_args()

    os_url = args.url
    insecure = args.insecure

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    indices_filename = args.input
    indices_list = read_json(indices_filename)

    fields = args.fields
    script = compose_script(fields)

    for indices in indices_list:
        source = indices["source"]
        dest = indices["dest"]
        logger.info("Reindex from {} to {}".format(source, dest))
        try:
            src_definition = fetch_index_definition(os_url, source, insecure)
            definition = {
                "mappings": src_definition["mappings"]
            }
            if args.aliases:
                definition["aliases"] = src_definition["aliases"]
            create_index(os_url, dest, definition, insecure)
            run_reindex(os_url, source, dest, script, insecure)
            logger.info("Done")
        except requests.exceptions.HTTPError as e:
            logger.error(e)
            continue
    logger.info("Check the all status: GET /_tasks?detailed=true&actions=*reindex&group_by=parents")


def parse_args():
    description = """
    Reindex a list of source indices to destination indices. Data, mappings,
    and aliases (optional) are copied. All but the index settings.
    """
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=description)

    parser.add_argument("url",
                        help="OpenSearch URL")
    parser.add_argument("input",
                        help="Indices JSON file")
    parser.add_argument("--insecure",
                        action='store_true',
                        help="Disable SSL verification")
    parser.add_argument("--aliases",
                        action='store_true',
                        help="Include the aliases")
    parser.add_argument("--fields",
                        nargs='+', default=[],
                        help="New fields to add in the reindex process ([KEY:VALUE KEY:VALUE ...])")
    parser.add_argument("-g", "--debug",
                        action='store_true',
                        help="Debug mode")
    args = parser.parse_args()
    return args


def read_json(file_name):
    with open(file_name, "r") as f:
        return json.loads(f.read())


def compose_script(fields):
    script = ""
    if not fields:
        return script

    script_source = ""
    for field in fields:
        key = field.split(":")[0]
        value = field.split(":")[1]
        script_source += SCRIPT_SOURCE % (key, value)
    script = SCRIPT % script_source

    return script


def fetch_index_definition(url, index, insecure):
    logger.debug("Fetching index definition from: {}".format(index))
    headers = {HEADER_KEY: HEADER_VALUE}
    session = create_session(headers, insecure)
    url_index = "{}/{}".format(url, index)
    r = session.get(url_index)

    return r.json()[index]


def create_index(url, index, definition, insecure):
    logger.debug("Creating a new index: {}".format(index))
    headers = {HEADER_KEY: HEADER_VALUE}
    session = create_session(headers, insecure)
    url_create = "{}/{}".format(url, index)
    r = session.put(url_create, data=json.dumps(definition))
    logger.debug("{}".format(r.json()))


def run_reindex(url, source, dest, script, insecure):
    headers = {HEADER_KEY: HEADER_VALUE}
    session = create_session(headers, insecure)
    reindex_url = REINDEX_URL.format(url)
    query = QUERY % (source, dest, script)
    r = session.post(reindex_url, data=query)
    logger.debug("Reindex: {}".format(r.json()))
    logger.debug("Check the status: GET /_tasks/{}".format(r.json()["task"]))


def create_session(headers, insecure=False):

    session = requests.Session()
    session.headers.update(headers)

    retries = urllib3.util.Retry(total=MAX_RETRIES,
                                 connect=MAX_RETRIES,
                                 status=MAX_RETRIES,
                                 backoff_factor=DEFAULT_SLEEP_TIME,
                                 raise_on_status=True)

    session.mount('http://', requests.adapters.HTTPAdapter(max_retries=retries))
    session.mount('https://', requests.adapters.HTTPAdapter(max_retries=retries))

    if insecure:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session.verify = False

    return session


if __name__ == '__main__':
    main()
