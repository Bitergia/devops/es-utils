# ElasticSearch/OpenSearch utils

Scripts to handle data in Elasticsearch databases

### fetch_indexes_count.py

Fetch the number of documents of all indexes from `setup.cfg` or `GET _cat/indices`
(OpenSearch/ElasticSearch) and write the result on a CSV file (by default 'output.csv').
```
$ python3 fetch_indexes_count.py -h
usage: fetch_indexes_count.py [-h] [--csv_output CSV_OUTPUT] [--setup SETUP]
                              url user_admin password_admin

positional arguments:
  url                   OpenSearch/ElasticSearch URL
  user_admin            OpenSearch/ElasticSearch user
  password_admin        OpenSearch/ElasticSearch password

optional arguments:
  -h, --help            show this help message and exit
  --csv_output CSV_OUTPUT
                        CSV output file (by default output.csv)
  --setup SETUP         Mordred setup
```

### remove_es_items.py

Remove specific items from an elasticsearch index
```
$ python3 remove_es_items.py -h
usage: remove_es_items.py [-h] host index field items [items ...]

positional arguments:
  host        ES host
  index       ES index name
  field       field of the ES index
  items       list of values ​​in the ES index field to remove

optional arguments:
  -h, --help  show this help message and exit

```

### update_by_query.py

Update OpenSearch documents using `update_by_query`
```
$ python3 update_by_query.py --help
usage: update_by_query.py [-h] [--verify]
                          host old_value new_value field indexes [indexes ...]

positional arguments:
  host        OpenSearch host including user and password if needed
  old_value   Old value
  new_value   New value
  field       Index field
  indexes     OpenSearch indexes

optional arguments:
  -h, --help  show this help message and exit
  --verify
```

# Reindex process

Follow these steps to reindex:
- `fetch_indices_list.py`: Create the indices JSON file to reindex (source, dest) filtering by tenant.
- `reindex.py`: Run reindex given a indexes JSON file.
- `switch_aliases.py`: Switch all aliases including filters given an indexes JSON file.
- `create_aliases.py`: Create new aliases using the tenant as a prefix (including filters) given an
  indices JSON file.
- `delete_source_indices.py`: Delete source indices given an indices JSON file.

NOTE: Depending on your needs you will need `switch_aliases.py` or `create_aliases.py`

### fetch_indices_list.py

Create an indices JSON file by listing indices from an OpenSearch instance and filtering by tenant.
Each item of the list will include the original source index name and a destination index name.
This latter name will be the same as the former. Except for the first token, that will be removed if
it matches the `--prefix` argument (by default is `temp`). Otherwise the index will be ignored entirely.

The output JSON file is a list of `source` and the `dest` indexes.
```
[
    {
        "source": "<prefix>_index",
        "dest": "index"
    }
]
```

For instance, if we have these indexes:
- `temp_bap_bitergia_git_220902_enriched_220902`
- `custom_bap_bitergia_git_220902_enriched_220902`

The second index will be ignored due to the prefix is not `temp`, but if we set
`--prefix custom` then it will ignore the first one.

Execution example:
```
python3 fetch_indices_list.py http://localhost:9200 bitergia
```

Output `bitergia.json`
```
[
    {
        "source": "temp_bap_bitergia_git_220902_enriched_220902",
        "dest": "bap_bitergia_git_220902_enriched_220902"
    }
]
```

### reindex.py

Reindex a list of source indices to destination indices from a JSON file.
Data, mappings, and aliases (optional) are copied. All but the index settings.
You can add new fields to the destination indices.

The JSON file can be created manually or running `fetch_indices_list.py`.

To add new fields on the reindex process add the `--fields` argument this way
`--fields field1:value1 field2:value2 ...`

Execution example:

- Run the script using `bitergia.json`
```
python3 reindex.py http://localhost:9200 bitergia.json --fields cm_tenant:bitergia --aliases
```

The index `bap_bitergia_git_220902_enriched_220902` will have:
- A new field `cm_tenant` with value `bitergia`
- The same data, mappings and aliases than the source index. 

### switch_aliases.py

Switch aliases automatically given an indices JSON file (created manually or
running `fetch_indices_list.py`). It will switch all aliases that each
old index had (including the filters) to its destination index.

Execution example:

- Before
```
GET _cat/aliases
affiliations            temp_bap_bitergia_git_220902_enriched_220902
git                     temp_bap_bitergia_git_220902_enriched_220902
```

- Run the script using `bitergia.json`
```
python3 switch_aliases.py http://localhost:9200 bitergia.json
```

- After
```
GET _cat/aliases
affiliations            bap_bitergia_git_220902_enriched_220902
git                     bap_bitergia_git_220902_enriched_220902
```

### create_aliases.py

Create aliases automatically given an indices JSON file (created manually or
running `fetch_indices_list.py`). It will create all aliases that each
old index had (including the filters) to its destination index, but including
the tenant as a prefix of the new aliases name.

Execution example:

- Before
```
GET _cat/aliases
affiliations            temp_bap_bitergia_git_220902_enriched_220902
git                     temp_bap_bitergia_git_220902_enriched_220902
```

- Run the script using `bitergia.json`
```
python3 create_aliases.py http://localhost:9200 bitergia bitergia.json
```

- After
```
GET _cat/aliases
affiliations            temp_bap_bitergia_git_220902_enriched_220902
git                     temp_bap_bitergia_git_220902_enriched_220902

bitergia_affiliations   bap_bitergia_git_220902_enriched_220902
bitergia_git            bap_bitergia_git_220902_enriched_220902
```

### delete_source_indices.py

Remove all source indices given an indices JSON file (created manually or
running `fetch_indices_list.py`)

- Before
```
GET _cat/indices
temp_bap_bitergia_git_220902_enriched_220902
bap_bitergia_git_220902_enriched_220902
```

- Run the script using `bitergia.json`
```
python3 delete_source_indices.py http://localhost:9200 bitergia.json
```

- After
```
GET _cat/indices
bap_bitergia_git_220902_enriched_220902
```

## Import/Export OpenSearch menu

The `menu_manager.py` script allows you to export and import menu/metadashboard given the URL and tenant.

NOTE: The `--menu_field` by default is `menu`, use `metadashboard` for bap instances < `0.5.0`.

Example:

- export
```
python3 menu_manager.py export https://admin:admin@localhost:9200 bitergia -o bitergia.json --menu_field metadashboard
```

- import
```
python3 menu_manager.py import https://admin:admin@localhost:9200 bitergia bitergia.json --menu_field menu
```

# Mordred files

This section manage Mordred configuration files (`setup.cfg`, `projects.json`, and `aliases.json`)

### add_meta_fields.py

Add meta fields to Mordred projects.json. The `fields` argument value must have
this format `[KEY:VALUE KEY:VALUE ...]`.

If the `meta` key does not exist on the project, it will create it with the new
fields or add them otherwise.

Execution example:

- before
```
{
    "Bitergia": {
        "git": [
            "https://github.com/chaoss/grimoirelab.git"
        }
    }
}
```

- Run the script with new meta field `tenant:bitergia`
```
python3 add_meta_fields.py projects.json tenant:bitergia
```

- After
```
{
    "Bitergia": {
        "git": [
            "https://github.com/chaoss/grimoirelab.git"
        },
        "meta": {
            "tenant": "bitergia"
        }
    }
}
```

### update_setup.py

Update Mordred setup.cfg. It will rename all indices/aliases using the new schema.

- From: <backend>_<project>_<body>
- To: bap_<tenant>_<backend>_<body>

But if the index is an alias (when the len of the index split by `_` is < 3).
- From: <alias>
- To: <tenant>_<alias>

Execution example:

- before
```
[git]
enriched_index = git_bitergia_220902_enriched_220902
```

- Run the script
```
python3 update_setup.py setup.cfg bitergia
```

- after
```
[git]
enriched_index = bap_bitergia_git_220902_enriched_220902
```

### add_tenant_aliases.py

Update aliases by adding the `tenant` prefix.

```
NOTE: If you do not set the argument `--output` (`-o`) the output file will overwrite
the input file
```

Example execution

- before `aliases.json`:
```
{
  "askbot": {
    "raw": [
      {
        "alias": "askbot-raw"
      }
    ],
    "enrich": [
      {
        "alias": "askbot"
      },
      {
        "alias": "affiliations"
      },
      {
        "alias": "all_enriched"
      }
    ]
  }
}
```

- Run script (`tenant = bitergia`)
```
python3 add_tenant_aliases.py aliases.json bitergia
```

- After `aliases.json`
```
{
  "git": {
    "raw": [
      {
        "alias": "bitergia_git-raw"
      }
    ],
    "enrich": [
      {
        "alias": "bitergia_git"
      },
      {
        "alias": "bitergia_affiliations"
      },
      {
        "alias": "bitergia_all_enriched"
      }
    ]
  }
}
```


# Migrate OpenSearch Saved Objects

### migrate_saved_objects.py

This script migrates OpenSearch Saved Objects (index-pattern and visualizations) to
adapt them to a multi-tenancy OpenSearch environment.

It will add the `tenant` as a prefix to all the index patterns and the index name in
`timelion` visualizations.

Index pattern (`tenant = bitergia`):
- `affiliations`: `bitergia_affiliations`
- `git`: `bitergia_git`
- `github_issue`: `bitergia_github_issue`

Timelion (`tenant = bitergia`):
- original
```
.es(index=git, timefield=grimoire_creation_date, metric=cardinality:repo_name)
```
- migrated
```
.es(index=bitergia_git, timefield=grimoire_creation_date, metric=cardinality:repo_name)
```

Example execution:

- Export all saved objects items from your OpenSearch instance (`export.ndjson`).
  - `Stack Management` -> `Saved objects` -> `Export <number> objects`
  - http://localhost:5601/app/management/opensearch-dashboards/objects
- Run the script
```
python3 migrate_saved_objects.py export.ndjson bitergia -o migrated.ndjson
```
- Import the migrated ndjson file (`migrated.ndjson`).
  - `Stack Management` -> `Saved objects` -> `Import`
  - http://localhost:5601/app/management/opensearch-dashboards/objects
