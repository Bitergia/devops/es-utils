#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import urllib3

import requests

DEFAULT_SLEEP_TIME = 1
MAX_RETRIES = 5

HEADER_KEY = "Content-Type"
HEADER_VALUE = "application/json"

INDICES_URL = "/_cat/indices"

DEFAULT_PREFIX = "temp"


def main():
    args = parse_args()
    os_url = args.url
    tenant = args.tenant
    prefix = args.prefix

    file_name = tenant + ".json"
    if args.output:
        file_name = args.output

    headers = {HEADER_KEY: HEADER_VALUE}
    session = create_session(headers, args.insecure)
    url = os_url + INDICES_URL
    r = session.get(url)
    data = r.text
    indices = compose_indices(data, tenant, prefix)

    write_json(file_name, indices)


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("url",
                        help="OpenSearch URL")
    parser.add_argument("tenant",
                        help="Only fetch indices from this tenant")
    parser.add_argument("--insecure",
                        action='store_true',
                        help="Disable SSL verification")
    parser.add_argument("--output",
                        help="Output JSON file (by default is the same value of tenant.json)")
    parser.add_argument("--prefix",
                        default=DEFAULT_PREFIX,
                        help="The name of the prefix to remove from the source index (by default is 'temp')")
    args = parser.parse_args()
    return args


def create_session(headers, insecure=False):

    session = requests.Session()
    session.headers.update(headers)

    retries = urllib3.util.Retry(total=MAX_RETRIES,
                                 connect=MAX_RETRIES,
                                 status=MAX_RETRIES,
                                 backoff_factor=DEFAULT_SLEEP_TIME,
                                 raise_on_status=True)

    session.mount('http://', requests.adapters.HTTPAdapter(max_retries=retries))
    session.mount('https://', requests.adapters.HTTPAdapter(max_retries=retries))

    if insecure:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session.verify = False

    return session


def compose_indices(data, tenant, prefix):
    indices = []
    for line in data.split("\n"):
        if not line:
            continue

        # The index name is on the third position (split by white space)
        # open  green   prefix_bap_tenant_*
        index = line.split()[2]
        index_split = index.split("_")
        try:
            index_tenant = index_split[2]
        except IndexError:
            continue

        if index_split[0] != prefix:
            continue

        if index_tenant == tenant:
            new = {
                "source": index,
                "dest": index.replace(prefix + "_", "")
            }
            indices.append(new)

    return indices


def write_json(file_name, data):
    with open(file_name, "w+") as f:
        f.write(json.dumps(data, indent=4))


if __name__ == '__main__':
    main()
