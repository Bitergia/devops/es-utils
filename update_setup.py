#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import configparser
import json
import logging


# General sections
GENERAL_SECTIONS = ["general", "projects", "es_collection", "es_enrichment",
                    "sortinghat", "phases", "panels"]

# Option index keys
OPTION_INDEX_KEYS = ["raw_index", "enriched_index", "in_index", "out_index"]

# Logging formats
LOG_FORMAT = "[%(asctime)s - %(levelname)s] - %(message)s"


logger = logging.getLogger(__name__)


def main():
    args = parse_args()
    setup = args.setup

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    logger.info("Migrate setup index starting...")
    config = configparser.ConfigParser()
    config.read(setup)

    for section in config.sections():
        if section in GENERAL_SECTIONS:
            continue
        logger.debug("Backend: {}".format(section))
        update_index(config, section, args.tenant)

    write_setup(config, setup)
    logger.info("Finished")


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("setup",
                        help="Input setup.cfg file")
    parser.add_argument("tenant",
                        help="Tenant")
    parser.add_argument("-g", "--debug",
                        action='store_true',
                        help="Debug mode")
    args = parser.parse_args()
    return args


def read_json(file_name):
    with open(file_name, "r") as f:
        return json.loads(f.read())


def update_index(config, backend, tenant):
    for option in OPTION_INDEX_KEYS:
        logger.debug("Option: {}".format(option))
        if not config.has_option(backend, option):
            logger.debug("Backend {} without {} option".format(backend, option))
            continue
        index = config.get(backend, option)
        index_migrated = index_new_schema(index, tenant)
        config.set(backend, option, index_migrated)
        logger.info("Updated index from {} to {}".format(index, index_migrated))


def index_new_schema(index, tenant):
    """ Update the index name to the new schema.
    - From: <backend>_<project>_<body>
    - To: bap_<tenant>_<backend>_<body>

    But if the index is an alias (when the len of the index split by `_` is < 3).
    - From: <alias>
    - To: <tenant>_<alias>

    :param index: index name
    :param tenant: tenant

    :return: index with the new schema
    """

    logger.debug("Index with old schema: {}".format(index))
    split_index = index.split("_")
    if len(split_index) < 3:
        new_index = "{}_{}".format(tenant, index)
    else:
        backend = split_index[0]
        body = "_".join(split_index[2:])
        new_index = "bap_{}_{}_{}".format(tenant, backend, body)
    logger.debug("Index with new schema: {}".format(new_index))
    return new_index


def write_setup(config, file):
    with open(file, 'w') as configfile:
        config.write(configfile)


if __name__ == '__main__':
    main()
