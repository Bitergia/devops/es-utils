#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import configparser
import csv

import requests
from urllib.parse import urljoin
import urllib3


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

HEADERS = {"Content-Type": "application/json"}
HEADER_TABLE = ["index", "documents"]
MORDRED_SECTIONS = ["general", "sortinghat", "phases", "panels"]


def main():
    args = parse_args()
    os_url = args.url
    os_user_admin = args.user_admin
    os_pass_admin = args.password_admin

    os_url = os_url if os_url.endswith('/') else os_url + "/"
    creds = "{}:{}".format(os_user_admin, os_pass_admin)
    url = os_url.replace("://", "://" + creds + "@")

    setup = args.setup if args.setup else None
    if setup:
        indexes = get_indexes_from_setup(setup)
    else:
        indexes = get_indexes(url)

    indexes_count = {}
    for index in indexes:
        indexes_count[index] = get_documents(url, index)

    csv_output = args.csv_output
    with open(csv_output, 'w', newline='') as csv_file:
        write_csv(csv_file, indexes_count)


def parse_args():
    """Parse command line arguments."""

    parser = argparse.ArgumentParser()
    parser.add_argument("url",
                        help="OpenSearch/ElasticSearch URL")
    parser.add_argument("user_admin",
                        help="OpenSearch/ElasticSearch user")
    parser.add_argument("password_admin",
                        help="OpenSearch/ElasticSearch password")
    parser.add_argument("--csv_output", default="output.csv",
                        help="CSV output file (by default output.csv)")
    parser.add_argument("--setup",
                        help="Mordred setup")

    return parser.parse_args()


def get_indexes_from_setup(setup):
    """Get a list of indexes from mordred setup.cfg file

    :param setup: mordred setup.cfg

    :return: indexes
    """
    config = configparser.ConfigParser()
    config.read(setup)
    indexes = []
    for section in config.sections():
        if section in MORDRED_SECTIONS:
            continue
        if config.has_option(section, "raw_index"):
            index = config.get(section, "raw_index")
            indexes.append(index)
        if config.has_option(section, "enriched_index"):
            index = config.get(section, "enriched_index")
            indexes.append(index)
        if config.has_option(section, "in_index"):
            index = config.get(section, "in_index")
            indexes.append(index)
        if config.has_option(section, "out_index"):
            index = config.get(section, "out_index")
            indexes.append(index)
    return indexes


def get_indexes(os_url):
    """Get a list of indexes executing 'GET _cat/indices'

    :param os_url: OpenSearch/ElasticSearch URL

    :return: indexes
    """
    url = urljoin(os_url, "_cat/indices")
    r = run_cmd(url)
    indexes_str = r.text.split("\n")
    indexes = []
    for index_str in indexes_str:
        index = index_str.split()
        if len(index) > 0:
            indexes.append(index[2])

    return indexes


def get_documents(os_url, index):
    """Get documents executing 'GET index/_count'

    :param os_url: OpenSearch/ElasticSearch URL
    :param index: index name

    :return: number of documents
    """
    url = urljoin(os_url, "{}/_count".format(index))
    try:
        r_raw = run_cmd(url)
        count = r_raw.json()['count']
    except requests.exceptions.HTTPError:
        count = None
    return count


def run_cmd(url, data=None):
    r = requests.get(url, headers=HEADERS, verify=False, data=data)
    r.raise_for_status()
    return r


def write_csv(csv_file, data):
    writer = csv.writer(csv_file, delimiter=',',
                        quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(HEADER_TABLE)
    for d in sorted(data.keys()):
        row = [d, data[d]]
        writer.writerow(row)


if __name__ == '__main__':
    """This script fetch the number of documents of all indexes from `setup.cfg` or
    `GET _cat/indices` (OpenSearch/ElasticSearch) and write the result on a CSV file
    (by default 'output.csv').

    $ python3 fetch_indexes_count.py OS_URL OS_USER OS_PASSWORD --csv_output indexes.csv
    """
    main()
