#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import logging

import ndjson


# Logging formats
LOG_FORMAT = "[%(asctime)s - %(levelname)s] - %(message)s"


logger = logging.getLogger(__name__)


def main():
    args = parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    logger.info("Saved objects migration starting...")

    saved_objects = read_file(args.input)
    update_saved_objects(saved_objects, args.tenant)
    write_file(args.output, saved_objects)

    logger.info("Done")


def parse_args():

    parser = argparse.ArgumentParser(description='Import and export OpenSearch menu')
    parser.add_argument('-g', '--debug',
                        action='store_true',
                        help='Active debug mode')
    parser.add_argument('input',
                        help='Input ndjson file')
    parser.add_argument('tenant',
                        help='Tenant name, the new index pattern will be <tenant>_git')
    parser.add_argument('-o', '--output',
                        default='output.ndjson',
                        help='Migrated ndjson file (by default "output.ndjson")')

    args = parser.parse_args()
    return args


def read_file(file):
    logger.debug("Reading: {}".format(file))
    with open(file, 'r') as f:
        data = ndjson.load(f)
        return data


def update_saved_objects(data, tenant):
    for entry in data:
        if 'type' not in entry:
            continue

        if entry['type'] == 'index-pattern':
            logger.debug("Updating index pattern: {}".format(entry['attributes']['title']))
            update_index_pattern(entry, tenant)
        elif entry['type'] == 'visualization' and is_timelion(entry['attributes']['visState']):
            logger.debug("Updating timelion: {}".format(entry['attributes']['title']))
            visState = update_timelion(entry['attributes']['visState'], tenant)
            entry['attributes']['visState'] = visState


def update_index_pattern(entry, tenant):
    title = entry['attributes']['title']
    new_title = add_tenant(tenant, title)
    entry['attributes']['title'] = new_title
    logger.debug("New index pattern title: {}".format(new_title))


def is_timelion(data):
    if not data:
        return False
    data_json = json.loads(data)
    return data_json['type'] == 'timelion'


def update_timelion(data, tenant):
    data_json = json.loads(data)
    expression = data_json['params']['expression']
    lines = ['']
    for line in expression.split(".es("):
        if not line:
            continue
        try:
            index_name = line.split("index=")[1].split(',')[0].split(')')[0]
            new_index_name = add_tenant(tenant, index_name)

            line = line.replace("index="+index_name, "index="+new_index_name)
            lines.append(line)
        except IndexError:
            logger.debug("Timelion expression without index: '{}'".format(line))
            continue
    new_expression = ".es(".join(lines)
    data_json['params']['expression'] = new_expression
    logger.debug("New timelion: {}".format(json.dumps(data_json)))
    return json.dumps(data_json)


def add_tenant(tenant, name):
    new_name = "{}_{}".format(tenant, name).replace('"', '')
    return new_name


def write_file(file, data):
    logger.info("Output file: {}".format(file))
    with open(file, 'w') as f:
        ndjson.dump(data, f)


if __name__ == '__main__':
    main()
