#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2024 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#

import argparse

import urllib3
import requests

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

UPDATE_BY_QUERY_URL = "{}/{}/_update_by_query?conflicts=proceed&pretty=true"
UPDATE_BY_QUERY = """{
  "query": {
    "term": {
      "%s": "%s"
    }
  },
  "script": {
    "lang": "painless",
    "source": "if (ctx._source.%s == params.old) { ctx._source.%s = params.new }",
    "params": {
      "old": "%s",
      "new": "%s"
    }
  }
}"""


def main():
    args = args_parser()

    host = args.host
    new_value = args.new_value
    old_value = args.old_value
    field = args.field
    indexes = args.indexes
    verify = args.verify

    for index in indexes:
        url = UPDATE_BY_QUERY_URL.format(host, index)
        data = UPDATE_BY_QUERY % (field, old_value, field, field, old_value, new_value)
        print("POST {}".format(url))
        print(data)
        r = run_requests(url, data, verify)
        print(r.text)
        print("---------------------------")


def args_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("host",
                        help="OpenSearch host including user and password if needed")
    parser.add_argument("old_value",
                        help="Old value")
    parser.add_argument("new_value",
                        help="New value")
    parser.add_argument("field",
                        help="Index field")
    parser.add_argument("indexes",
                        nargs="+",
                        help="OpenSearch indexes")
    parser.add_argument("--verify",
                        action="store_true",
                        default=False)
    args = parser.parse_args()

    return args


def run_requests(url, data, verify):
    headers = {"Content-Type": "application/json"}
    r = requests.post(url, headers=headers, data=data, verify=verify)
    r.raise_for_status()
    return r


if __name__ == '__main__':
    main()
