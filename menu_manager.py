#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Quan Zhou <quan@bitergia.com>
#


import argparse
import json
import logging
import urllib3

import requests


DEFAULT_SLEEP_TIME = 1
MAX_RETRIES = 5

DEFAULT_TENANT = ["global", "global_admin"]

HEADER_KEY = "Content-Type"
HEADER_VALUE = "application/json"


KIBANA_MENU_URL = "{}/.kibana/_doc/{}"

KIBANA_MAPPING = "{}/.kibana/_mapping"
DYNAMIC_MAPPING = '{"dynamic": "true"}'

# Logging formats
LOG_FORMAT = "[%(asctime)s - %(levelname)s] - %(message)s"


logger = logging.getLogger(__name__)


def main():
    args = parse_args()
    insecure = args.insecure

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    if args.command == 'import':
        logger.info("Import menu {} to the tenant {}".format(args.menu, args.tenant))
        menu = read_file(args.menu)
        import_menu(args.url, args.tenant, menu, args.menu_field, insecure)
    elif args.command == 'export':
        logger.info("Export menu {} from the tenant {}".format(args.outfile, args.tenant))
        export_menu(args.url, args.tenant, args.outfile, args.menu_field, insecure)
    logger.info("Done")


def parse_args():

    parser = argparse.ArgumentParser(description='Import and export OpenSearch menu')
    parser.add_argument('--insecure',
                        action='store_true',
                        help='Disable ssl connection verification')
    parser.add_argument('-g', '--debug',
                        action='store_true',
                        help='Active debug mode')
    subparsers = parser.add_subparsers(dest='command')
    subparsers.required = True
    # Import
    import_parser = subparsers.add_parser('import',
                                          help='Create a menu from a file')
    import_parser.add_argument('url',
                               help='OpenSearch URL')
    import_parser.add_argument('tenant',
                               help='Tenant to import menu.')
    import_parser.add_argument('menu',
                               help="Menu JSON file to import")
    import_parser.add_argument('--menu_field',
                               default='menu', choices=['menu', 'metadashboard'],
                               help='The name of the menu field (by default: menu,'
                                    'use metadashboard for bap < 0.5.0)')
    # Export
    export_parser = subparsers.add_parser('export',
                                          help='Export the menu into file')
    export_parser.add_argument('url',
                               help='Base API URL')
    export_parser.add_argument('tenant',
                               help='Tenant to export menu from')
    export_parser.add_argument('--outfile', '-o',
                               default='menu.json', dest='outfile',
                               help='Export menu JSON file (by default is menu.json)')
    export_parser.add_argument('--menu_field',
                               default='menu', choices=['menu', 'metadashboard'],
                               help='The name of the menu field (by default: menu,'
                                    'use metadashboard for bap < 0.5.0)')

    args = parser.parse_args()
    return args


def import_menu(url, tenant, menu_content, menu_field, insecure):
    activate_dynamic_mapping(url, tenant, insecure)
    menu = {menu_field: menu_content}
    update_menu(url, tenant, menu, menu_field, insecure)


def export_menu(url, tenant, file, menu_field, insecure):
    menu = fetch_menu(url, tenant, menu_field, insecure)
    write_file(file, menu[menu_field])


def fetch_menu(base_url, tenant, menu_field, insecure):
    logger.debug("Fetching {} ...".format(menu_field))
    if tenant in DEFAULT_TENANT:
        tenant = None
    headers = {
        HEADER_KEY: HEADER_VALUE,
        "securitytenant": tenant
    }
    session = create_session(headers, insecure)
    url = KIBANA_MENU_URL.format(base_url, menu_field)
    r = session.get(url)
    logger.debug(r.json())

    return r.json()["_source"]


def activate_dynamic_mapping(base_url, tenant, insecure):
    logger.debug("Activate dynamic mapping")
    if tenant in DEFAULT_TENANT:
        tenant = None
    headers = {
        HEADER_KEY: HEADER_VALUE,
        "securitytenant": tenant
    }
    session = create_session(headers, insecure)
    url = KIBANA_MAPPING.format(base_url)
    r = session.put(url, data=DYNAMIC_MAPPING)
    logger.debug(DYNAMIC_MAPPING)
    logger.debug(r.json())


def update_menu(base_url, tenant, menu, menu_field, insecure):
    logger.debug("Create {}".format(menu_field))
    if tenant in DEFAULT_TENANT:
        tenant = None
    headers = {
        HEADER_KEY: HEADER_VALUE,
        "securitytenant": tenant
    }
    session = create_session(headers, insecure)
    url = KIBANA_MENU_URL.format(base_url, menu_field)
    r = session.put(url, data=json.dumps(menu))
    logger.debug(menu)
    logger.debug(r.json())


def create_session(headers, insecure=False):

    session = requests.Session()
    session.headers.update(headers)

    retries = urllib3.util.Retry(total=MAX_RETRIES,
                                 connect=MAX_RETRIES,
                                 status=MAX_RETRIES,
                                 backoff_factor=DEFAULT_SLEEP_TIME,
                                 raise_on_status=True)

    session.mount('http://', requests.adapters.HTTPAdapter(max_retries=retries))
    session.mount('https://', requests.adapters.HTTPAdapter(max_retries=retries))

    if insecure:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session.verify = False

    return session


def read_file(file):
    logger.debug("Reading: {}".format(file))
    with open(file, 'r') as f:
        data = json.loads(f.read())
        logger.debug("{}".format(data))
        return data


def write_file(file, data):
    logger.debug("Write menu to {}".format(file))
    with open(file, 'w') as f:
        f.write(json.dumps(data, indent=4))


if __name__ == '__main__':
    main()
